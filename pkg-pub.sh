#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./vscode-asharp-0.0.6.vsix
npx ovsx publish vscode-asharp-0.0.6.vsix --debug --pat <PAT>
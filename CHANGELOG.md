# Changelog

## [0.0.6]

- canonical layout reorg

## [0.0.5]

- add missing support for .ads (ada spec) file extension

## [0.0.4]

- fix manual install instructions in readme

## [0.0.3]

- fix repo url in pkg

## [0.0.2]

- rename publisher
- add more manifest values
- fix content types xml

## [0.0.1]

- Initial release with syntax highlighting & code snippets

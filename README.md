# ASharp (A#) for VS Code

## About

    This Visual Studio Code extension is intended to support ASharp.Net (A#), which is Ada 2005 for .Net, with some syntax highlighting. Note: I was unable to get A# tools working on Windows Server 2003 or ReactOS, so I could not try out an actual A# app to verify the extension. So the extension was tested on an AdaCore app. Note that there is also a proper Ada extension from AdaCore (<https://marketplace.visualstudio.com/items?itemName=AdaCore.ada>), and that one could be used if this extension does not satisfy.

## Features

- Syntax highlighting
    (e.g. `for...loop`, `while`, `case...when`, `if...else...else if`, `while...loop`)
- Code snippets
    (e.g. `for...loop`, `while`, `case...when`, `if...else...else if`, `while...loop`)

## Installation

### From repository

Download the .vsix file and choose 'Install from VSIX...' from the app.

## Changelog

See the [changelog](https://gitlab.com/sjsepan/vscode-asharp/blob/HEAD/CHANGELOG.md) for details.

## Issues

-

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/15/2025
